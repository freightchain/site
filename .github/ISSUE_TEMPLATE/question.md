---
name: Question ❓
about: Usage question or discussion about the Carbon Design System Website.
title: ""
labels: "type: question ❓"
assignees: ""
---

<!--

please email: support@freight.zendesk.com
contact via telegram: https://t.me/freighttrust
or via twitter at: https://twitter.com/freighttrust

-->

## Summary

## Relevant information

<!-- Provide as much useful information as you can -->
