# Analytics

We use [Fathom](https://app.usefathom.com/share/KRZBORKB/freighttrust.com) for analytics tracking. Individual events can be fired off to track 'goals'. Ping the Carbon slack channel to add an event.
