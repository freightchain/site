import React from 'react';
import Footer from 'gatsby-theme-carbon/src/components/Footer';
import Packages from '../../../package.json';

const currentYear = new Date().getFullYear();

const version = Packages.dependencies['carbon-components'];
const reactVersion = Packages.dependencies['carbon-components-react'];

const Content = ({ buildTime }) => (
  <>
    <p>
      BSD & GPL Licensed 
      <br /> issues/source on {' '}
      <a
        style={{ textDecoration: 'underline' }}
        href="https://github.com/freight-chain"
      >
        GitHub.
      </a>
    </p>
    <p>
      1424 4th St Ste 214 PMB 1513
      <br />
      Santa Monica, CA 90401
      <br />
      <br />
      support@freight.zendesk.com 
      <br />
      <br />
      Last updated @ {buildTime} <br />
      Web Components Semantic {reactVersion} <br />
      Network Semantic {version} <br />
      <br />
      Copyright © {currentYear}  <br />
      <br />
      FreightTrust & Clearing Corporation
    </p>
  </>
);

const links = {
  firstCol: [
    {
      href: 'https://freight-chain.github.io/obm',
      linkText: 'Omnibus Documentation',
    },
    { href: 'https://www.github.com/freight-trust/legal/privacy', linkText: 'Privacy' },
    { href: 'https://www.github.com/freight-trust/legal/terms-of-service', linkText: 'Terms of use' },
    { href: 'https://freighttrust.com', linkText: 'Network' },
  ],
  secondCol: [
    { href: 'https://medium.com/freighttrust', linkText: 'Medium' },
    { href: 'https://twitter.com/freighttrustnet', linkText: 'Twitter' },
    {
      href: 'https://t.me/freighttrust',
      linkText: 'Telegram',
    },
  ],
};

const CustomFooter = () => <Footer links={links} Content={Content} />;

export default CustomFooter;
