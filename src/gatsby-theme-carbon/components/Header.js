import React from 'react';
import Header from 'gatsby-theme-carbon/src/components/Header';

const CustomHeader = (props) => (
  <Header {...props}>
    Freight Trust<span>Clearing</span>
  </Header>
);

export default CustomHeader;
