import React from 'react';
import { HomepageCallout, ResourceCard } from 'gatsby-theme-carbon';
import HomepageTemplate from 'gatsby-theme-carbon/src/templates/Homepage';
import { calloutLink } from './Homepage.module.scss';
import HomepageVideo from '../../components/HomepageVideo/HomepageVideo';

const FirstLeftText = () => <p>Freight Trust Network</p>;

const FirstRightText = () => (
  <p>
    An EDI <strong>smart contract layer </strong> protocol
    designed for <strong>performant</strong>, fault-tolerant
    enterprise system. Unlike legacy EDI transactions this
    system is built for integrated financial services.
  </p>
);

const SecondLeftText = () => (
  <p>
    Looking to test?
    <br />
    or want to find out more?
  </p>
);

const SecondRightText = () => (
  <p>
    The world as we know it runs on and depends on EDI. 
    Maidenlane is specifically designed for high-volume,
    complex transformation and validation of data across a
    range of different formats and standards
    <a
      className={calloutLink}
      href="https://freight.page.link/demo"
    >
      Request a Demo →
    </a>
  </p>
);

const customProps = {
  Banner: (
    <>
      <span className="homepage--dots" />
      <section className="homepage--header">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-2 bx--offset-lg-8 bx--offset-md-4 bx--offset-sm-2 homepage--tile-header">
              <ResourceCard
                subTitle="Read"
                title="Migration guide"
                href="/updates/migration-guide/overview"
                color="dark"
                actionIcon="arrowRight"
                onClick={() => fathom('trackGoal', '0GXPXZKE', 0)}
              />
            </div>
            <HomepageVideo />
          </div>
        </div>
      </section>
    </>
  ),
  FirstCallout: (
    <HomepageCallout
      backgroundColor="#030303"
      color="white"
      leftText={FirstLeftText}
      rightText={FirstRightText}
    />
  ),
  SecondCallout: (
    <HomepageCallout
      leftText={SecondLeftText}
      rightText={SecondRightText}
      color="white"
      backgroundColor="#061f80"
    />
  ),
};

// spreading the original props gives us props.children (mdx content)
function ShadowedHomepage(props) {
  return <HomepageTemplate {...props} {...customProps} />;
}

export default ShadowedHomepage;
