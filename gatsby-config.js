module.exports = {
  siteMetadata: {
    title: 'Freight Trust & Clearing',
    siteUrl: 'https://www.freighttrust.com',
    description:
      "Freight Trust and Clearing is an EDI enabled Hyplerdger Besu Network that utilizes a smart contract protocol to enable Suppply Chain Enterprises to integrate with the financial indsutry.",
    keywords:
      "Freight Logistics Supply Chain Blockchain Hyperledger Besu EDI transactions ledger transaction enterprise trucking maritime intermodal rail cargo bill of lading warehouse receipt commodities"
  },
  plugins: [
    'gatsby-theme-carbon-svgs',
    'gatsby-plugin-lodash',
    {
      resolve: 'gatsby-theme-carbon',
      options: {
        mdxExtensions: ['.mdx'],
        isSearchEnabled: true,
        iconPath: './src/images/favicon.svg',
        titleType: 'prepend',
        repository: {
          baseUrl: 'https://gitlab.com/freightchain/site/',
          subDirectory: '',
        },
      },
    },
    {
      resolve: 'gatsby-plugin-fathom',
      options: {
        siteId: 'KRZBORKB',
      },
    },
    'gatsby-plugin-netlify-cache',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-remove-serviceworker',
  ],
};
